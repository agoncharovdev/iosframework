//
//  CollectionViewCell.swift
//  Haneke
//
//  Created by Hermes Pique on 9/17/14.
//  Copyright (c) 2014 Haneke. All rights reserved.
//

import UIKit
import Haneke

class CollectionViewCell: UICollectionViewCell {
    
    var imageView : UIImageView!
    var fetcher:NetworkFetcher<UIImage>!
    var urlString:String!{
        didSet{
            
            if(fetcher != nil){
                fetcher.cancelFetch()
                imageView.image = nil
            }

            
            if(urlString != nil){
                
                let URL = NSURL(string:urlString)!
                
                fetcher = NetworkFetcher<UIImage>(URL: URL)
                
                Shared.imageCache.fetch(key: URL.absoluteString)
                
                Shared.imageCache.fetch(fetcher: fetcher, formatName: HanekeGlobals.Cache.OriginalFormatName, failure: nil) { [weak self](image:UIImage) in
                    
                    self?.imageView.image = image
                }
            }
            
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initHelper()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initHelper()
    }
    
    func initHelper() {
        imageView = UIImageView(frame: self.contentView.bounds)
        imageView.clipsToBounds = true
        imageView.contentMode = .ScaleAspectFill
        imageView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        self.contentView.addSubview(imageView)
    }
    
    override func prepareForReuse() {
        //imageView.hnk_cancelSetImage()
        //imageView.image = nil
    }
    
}
