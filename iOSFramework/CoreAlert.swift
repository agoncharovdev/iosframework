//
//  CoreAlert.swift
//  Yammy
//
//  Created by Alexey Honcharov on 03.03.16.
//  Copyright © 2016 Alexey Honcharov. All rights reserved.
//

import UIKit

public class CoreAlert: NSObject, UIPopoverPresentationControllerDelegate {
    
    public typealias AlertCloseHandler = (payload:AnyObject?) -> (Void)
    
    public var closeHandler:AlertCloseHandler?
    public var alertController:UIAlertController?
    public weak var vc:UIViewController?
    public weak var popoverSourceView:UIView?
    
    public final func addTo(vc:CoreController, popoverSourceView:UIView?){
        self.vc = vc
        self.popoverSourceView = popoverSourceView
        initAlert()
        initPopover()
        presentAlert()
    }
    
    public func initAlert(){
        fatalError("Must be overriden!")
    }
    
    public func initPopover(){
        fatalError("Must be overriden!")
    }
    
    public func presentAlert(){
        fatalError("Must be overriden!")
    }
    
    public func cleanUp(){
        alertController = nil
        closeHandler = nil
        vc = nil
        //onRemove()
        //dismissViewControllerAnimated(true, completion: nil)
    }
    
    // called when a Popover is dismissed
    public func popoverPresentationControllerDidDismissPopover(popoverPresentationController: UIPopoverPresentationController) {
        print("Popover was dismissed with external tap. Have a nice day!")
    }
    public func popoverPresentationControllerShouldDismissPopover(popoverPresentationController: UIPopoverPresentationController) -> Bool {
        // return YES if the Popover should be dismissed
        // return NO if the Popover should not be dismissed
        return true
    }
    public func popoverPresentationController(popoverPresentationController: UIPopoverPresentationController, willRepositionPopoverToRect rect: UnsafeMutablePointer<CGRect>, inView view: AutoreleasingUnsafeMutablePointer<UIView?>) {
        
        // called when the Popover changes positon
    }
    public func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle{
        return UIModalPresentationStyle.None
    }
}

