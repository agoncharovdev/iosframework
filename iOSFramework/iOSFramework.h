//
//  iOSFramework.h
//  iOSFramework
//
//  Created by Alexey Honcharov on 08.06.16.
//  Copyright © 2016 Alexey Honcharov. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for iOSFramework.
FOUNDATION_EXPORT double iOSFrameworkVersionNumber;

//! Project version string for iOSFramework.
FOUNDATION_EXPORT const unsigned char iOSFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <iOSFramework/PublicHeader.h>


