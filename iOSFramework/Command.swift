//
//  Command.swift
//  Yammy
//
//  Created by Alexey Honcharov on 11.02.16.
//  Copyright © 2016 Alexey Honcharov. All rights reserved.
//

import Foundation

public typealias CommandResultHandler = (success:Bool, result:AnyObject?) -> (Void)

public class Command : NSObject {
    
    override public init(){
        super.init()
        onInitialized()
    }
    
    final public func getService(serviceClass:AnyClass)->Service{
        return Service.getService(serviceClass)
    }
    
    public func onInitialized(){
        
    }
    
    public func execute(resultHandler:CommandResultHandler){
        
    }
    
    public func stop(){
        
    }
    
    public func remove(){
        stop()
    }
}
