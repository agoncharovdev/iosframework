//
//  CoreController.swift
//  Yammy
//
//  Created by Alexey Honcharov on 17.02.16.
//  Copyright © 2016 Alexey Honcharov. All rights reserved.
//

import UIKit


public class CoreController: UIViewController, UITextViewDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate {

    public var proxy:Proxy?
    
    deinit{
        Proxy.proxyInstances.removeObject(proxy)
    }
    
    public func getProxyType()->AnyClass?{
        return nil
    }
    
    public final func sendProxyAction(action:String, payload:AnyObject?){
        self.proxy?.onOwnerAction(action, payload: payload)
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        if(getProxyType() != nil){
            self.proxy = Proxy.createProxyByType(getProxyType()!, owner: self)
        }
        else{
            print("Can not initProxy - NO PROXY FOR THIS CONTROLLER")
        }
    }
    
    public final func setPopGestureEnabled(isEnabled:Bool){
        self.navigationController?.interactivePopGestureRecognizer?.delegate = isEnabled ? self : nil
    }
    
    public final func hideBackNavItemHidden(){
        let emptyBackButton = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        navigationItem.hidesBackButton = true
        navigationItem.backBarButtonItem = emptyBackButton
    }
    
    public final func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    internal func enterBackground(notification: NSNotification){
        print("ENTER BACKGROUND")
        proxy?.applicationDidEnterBackground()
    }
    
    internal func enterForeground(notification: NSNotification){
        print("ENTER FOREGROUND")
        proxy?.applicationDidBecomeActive()
    }
    
    internal func enterTerminate(notification: NSNotification){
        print("WILL TERMINATE")
        proxy?.applicationWillTerminate()
    }
    
//    final func deinitProxy(){
//        Core.proxyInstances.removeObject(self.proxy)
//        self.proxy?.remove()
//        self.proxy = nil
//    }
    
    override public func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        proxy?.onOwnerWillAppear()
    }
    
    override public func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        proxy?.onOwnerWillDisappear()
    }
    
    override public func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CoreController.enterBackground(_:)), name:UIApplicationDidEnterBackgroundNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CoreController.enterForeground(_:)), name:UIApplicationDidBecomeActiveNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CoreController.enterTerminate(_:)), name:UIApplicationWillTerminateNotification, object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CoreController.keyboardDidShowNotification(_:)), name: UIKeyboardDidShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CoreController.keyboardDidHideNotification(_:)), name: UIKeyboardDidHideNotification, object: nil)
        
        proxy?.onOwnerAppeared()
    }

    override public func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        
        NSNotificationCenter.defaultCenter().removeObserver(self)
        proxy?.onOwnerDisappeared()
    }
    
    public func enableTextfieldOffsetAnimation(){
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CoreController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CoreController.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
    }
    
    public func addDoneButtonTo(textField: UITextField) {
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action: #selector(CoreController.didTapDone(_:)))
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        textField.inputAccessoryView = keyboardToolbar
    }
    
    public func didTapDone(sender: AnyObject?) {
        self.view.endEditing(true)
    }
    
    // UITextView DELEGATE
    
    public func textViewDidChange(textView: UITextView) { //Handle the text changes here
        
    }
    public func textViewDidEndEditing(textView: UITextView) {
        
    }
    public func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    // KEYBOARD TEXTFIELD HANDLERS
    
    public var keyboardHeight: CGFloat = 0
    final public func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    public final func keyboardWillShow(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            if let keyboardSize =  (userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
                keyboardHeight = keyboardSize.height
                self.setViewOffset(true)
            }
        }
    }
    public var hideKeyboardTap:UITapGestureRecognizer?
    public override func keyboardDidShowNotification(notification: NSNotification) {
        super.keyboardDidShowNotification(notification)
        
        self.view.gestureRecognizers?.removeObject(hideKeyboardTap)
        
        hideKeyboardTap = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        self.view.addGestureRecognizer(hideKeyboardTap!)
    }
    public override func keyboardDidHideNotification(notification: NSNotification) {
        super.keyboardDidHideNotification(notification)
        
        self.view.gestureRecognizers?.removeObject(hideKeyboardTap)
    }
    
    public func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        return true // to not show keyboard - return false
    }
    public func textFieldDidEndEditing(textField: UITextField) {
    }
    public func textFieldDidBeginEditing(textField: UITextField) {
    }
    public final func keyboardWillHide(notification: NSNotification) {
        self.setViewOffset(false)
    }
    public func setViewOffset(isKeyboardShowing: Bool) {
        
        var textfieldGlobalY:CGFloat = 0
        var offset:CGFloat = 0
        
        if(isKeyboardShowing){
            
            if let firstResponder = self.view.getCurrentFirstResponder() where firstResponder is UITextField{
                let frFrame = firstResponder.convertRect(firstResponder.bounds, toView: self.view)
                textfieldGlobalY = frFrame.origin.y + frFrame.height*3
            }
            
//            if let firstResponder = self.view.getCurrentFirstResponder() where firstResponder is UITextView{
//                let frFrame = firstResponder.convertRect(firstResponder.bounds, toView: self.view)
//                var contentSize = firstResponder.contentSize.height
//                if contentSize > firstResponder.frame.size.height{
//                    contentSize = firstResponder.frame.size.height
//                }
//               
//                textfieldGlobalY = frFrame.origin.y + firstResponder.contentSize.height + 30
//                
//            }
            
            let keyboardTop = self.view.frame.height - keyboardHeight
            if(keyboardTop < textfieldGlobalY){
                offset = textfieldGlobalY - keyboardTop
            }
        }
        
        UIView.animateWithDuration(0.1, animations: {
            let newOrigin = CGPoint(x: 0, y: -offset)
            self.view.frame.origin = newOrigin
        })
    }
    
    override public func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        proxy?.prepareForSegue(segue)
    }
}


