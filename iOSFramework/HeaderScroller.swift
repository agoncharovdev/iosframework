import UIKit

public class HeaderScrollerView:UIImageView{
    
    public var imageLoader:ImageLoader?
    
    public var activityIndicator:UIActivityIndicatorView!
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        //setup()
    }
    
    required public init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        //setup()
    }
    
    override public func didMoveToSuperview() {
        super.didMoveToSuperview()
        setup()
    }
    public func setup(){
        self.opaque = false
        activityIndicator = UIActivityIndicatorView()
        activityIndicator.hidesWhenStopped = true
        
        self.addSubview(activityIndicator)
        
        self.autoresizingMask = [.FlexibleTopMargin, .FlexibleBottomMargin, UIViewAutoresizing.FlexibleHeight]
        
        self.contentMode = .ScaleAspectFill
        self.clipsToBounds = true
    }
    
    public func loadImage(){
        activityIndicator.startAnimating()
        imageLoader?.load({ [weak self, weak activityIndicator](loadedImage) -> (Void) in
            
            self?.image = loadedImage
            activityIndicator?.stopAnimating()
        })
    }
}
public class HeaderScroller: UIScrollView, UIScrollViewDelegate {
    
    public var currentScrollerView:HeaderScrollerView?
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required public init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    internal var imageView:UIImageView?
    public var backgroundImage:UIImage?{
        didSet{
            if (self.imageView == nil){
                self.imageView = UIImageView(frame:self.frame)
                self.imageView?.contentMode = .ScaleAspectFill
                self.insertSubview(imageView!, atIndex: 0)
                self.imageView?.autoresizingMask = [.FlexibleTopMargin, .FlexibleBottomMargin, UIViewAutoresizing.FlexibleHeight]
            }
            imageView?.image = self.backgroundImage
            imageView?.size = self.size
            imageView?.clipsToBounds = true
        }
    }
    
    public var topConstraint:NSLayoutConstraint!
    public var bottomConstraint:NSLayoutConstraint!
    
    public var headerScrollerViews:[HeaderScrollerView]?
    
    public func load(){
        if let headerViews = self.headerScrollerViews where headerViews.count > 0 {
            for view in headerViews{
                view.loadImage()
            }
        }
    }
    
    public func initialize(bg:UIImage?, imageLoaders:[ImageLoader]? = nil){
        
        headerScrollerViews = [HeaderScrollerView]()
        currentScrollerView = nil
        self.delegate = self
        
        self.superview!.clipsToBounds = true
        self.clipsToBounds = true
        self.showsHorizontalScrollIndicator = false
        self.showsVerticalScrollIndicator = false
        self.alwaysBounceVertical = false
        
        self.topConstraint = NSLayoutConstraint(item: self, attribute:
            .Top, relatedBy: .Equal, toItem: self.superview,
                  attribute: NSLayoutAttribute.Top, multiplier: 1.0,
                  constant: 0)
        self.bottomConstraint = NSLayoutConstraint(item: self, attribute:
            .Bottom, relatedBy: .Equal, toItem: self.superview,
                     attribute: NSLayoutAttribute.Bottom, multiplier: 1.0,
                     constant: 0)
        let leftConstraint = NSLayoutConstraint(item: self, attribute:
            .LeadingMargin, relatedBy: .Equal, toItem: self.superview,
                            attribute: NSLayoutAttribute.LeadingMargin, multiplier: 1.0,
                            constant: 0)
        let rightConstraint = NSLayoutConstraint(item: self, attribute:
            .TrailingMargin, relatedBy: .Equal, toItem: self.superview,
                             attribute: NSLayoutAttribute.TrailingMargin, multiplier: 1.0,
                             constant: 0)
        
        self.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activateConstraints([self.topConstraint, self.bottomConstraint, leftConstraint, rightConstraint])
        
        self.bounces = false
        self.pagingEnabled = true
        
        if let imageCount = imageLoaders?.count{
            
            let tableWidth = self.frame.width
            var allFrame = CGRectMake(0, 0, 0, 0)
            
            for index in 0..<imageCount{
                
                allFrame.origin.x = tableWidth * CGFloat(index)
                allFrame.size = CGSizeMake(tableWidth, self.frame.size.height)
                print(allFrame.origin.x, allFrame.origin.y, allFrame.size)
                
                let headerScrollView = HeaderScrollerView(frame: allFrame)
                headerScrollView.image = bg
                self.addSubview(headerScrollView)
                
                let imageLoader = imageLoaders![index]
                headerScrollView.imageLoader = imageLoader
                
                headerScrollerViews?.append(headerScrollView)
            }
            
            currentScrollerView = headerScrollerViews?.first
            
            self.contentSize = CGSizeMake(CGFloat(imageCount) * self.frame.width, self.frame.height)
            
        }
    }
    
    public func scrollViewDidScroll(scrollView: UIScrollView) {
        
        let currentPage = Int(scrollView.contentOffset.x / scrollView.frame.width)
        currentScrollerView = headerScrollerViews?.get(currentPage)
        
//        if scrollView.contentOffset.y >= 0 {
//            // scrolling up
//            self.superview!.clipsToBounds = true
//            
//            topConstraint.constant = scrollView.contentOffset.y / 2
//            bottomConstraint.constant = scrollView.contentOffset.y / 2
//        }
//        else {
//            // scrolling down
//            self.superview!.clipsToBounds = false
//            topConstraint.constant = scrollView.contentOffset.y
//            bottomConstraint.constant = 0 //10.0
//        }
    }
}
