//
//  SearchController.swift
//  Yammy
//
//  Created by Alexey Honcharov on 25.05.16.
//  Copyright © 2016 Alexey Honcharov. All rights reserved.
//

import UIKit

public class SearchController: CoreController {
    
    @IBOutlet public weak var searchBar: SearchBar!
    @IBOutlet public weak var searchBarHeightConstraint: NSLayoutConstraint!
    @IBOutlet public weak var table: UITableView!

    override public func viewDidLoad() {
        super.viewDidLoad()

        let fontSize = CGFloat(Device.size(small: 12, medium: 15, big: 18))
        searchBar.preferredFont = UIFont(name: "GothamPro", size: fontSize)
        searchBar.preferredTextColor = UIColor.whiteColor()
        searchBar.barTintColor = UIColor(hexString: "#c2ad61")
        searchBar.tintColor = UIColor.whiteColor()
        searchBar.showsBookmarkButton = false
        searchBar.showsCancelButton = true
    }

    deinit{
        print("SearchController DEINIT")
    }
}

public class SearchBar: UISearchBar, UITextFieldDelegate {
    
    public var preferredFont: UIFont!
    public var preferredTextColor: UIColor!
    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override public func drawRect(rect: CGRect) {
        // Drawing code
        
        loopRecursivelySubviewsWithFunc { (subview) in
            
            if subview.isKindOfClass(UITextField){
                
                let searchField = subview as! UITextField
                searchField.frame = CGRectMake(5.0, 5.0, self.frame.size.width - 10.0, self.frame.size.height - 10.0)
                searchField.font = self.preferredFont
                searchField.textColor = self.preferredTextColor
                searchField.backgroundColor = self.barTintColor
            }
            
            if subview.isKindOfClass(UIButton){
                
                let button = subview as! UIButton
                //button.adjustFont()
                button.imageTintColor(self.preferredTextColor)
                button.tintColor = self.preferredTextColor
                button.titleLabel?.font = self.preferredFont
                button.setTitleColor(self.preferredTextColor, forState: UIControlState.Normal)
            }
        }
        
        if let textFieldInsideSearchBar = self.valueForKey("searchField") as? UITextField,
            let glassIconView = textFieldInsideSearchBar.leftView as? UIImageView{
            
            textFieldInsideSearchBar.delegate = self
            
            textFieldInsideSearchBar.backgroundColor = UIColor(hexString: "#A89554")
            //Magnifying glass
            glassIconView.image = glassIconView.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
            glassIconView.tintColor = self.preferredTextColor
            
            if let clearButton = textFieldInsideSearchBar.valueForKey("clearButton") as? UIButton{
                clearButton.imageTintColor(self.preferredTextColor)
            }
        }
        
        let startPoint = CGPointMake(0.0, frame.size.height)
        let endPoint = CGPointMake(frame.size.width, frame.size.height)
        let path = UIBezierPath()
        path.moveToPoint(startPoint)
        path.addLineToPoint(endPoint)
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = path.CGPath
        shapeLayer.strokeColor = preferredTextColor.CGColor
        shapeLayer.lineWidth = 2.5
        
        layer.addSublayer(shapeLayer)
        
        super.drawRect(rect)
    }
    
    public func textFieldShouldReturn(textField: UITextField) -> Bool{
        textField.resignFirstResponder()
        return true
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        
        self.frame = frame
        
        searchBarStyle = UISearchBarStyle.Prominent
        translucent = false
    }
    
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
