
import UIKit
import MapKit
import CoreLocation

public enum LocationPickerVCActions:String{
    case SearchTextChanged = "SearchTextChanged", SelectLocation = "SelectLocation"
}

public class LocationPickerVC: CoreController, UITableViewDataSource, UITableViewDelegate {
    
	public struct CurrentLocationListener {
		let once: Bool
		let action: (CLLocation) -> ()
	}
    
	public let locationManager = CLLocationManager()
    
    public var historyLocations: [Location] = []
    public var currentLocations: [Location] = []
	
	var currentLocationListeners: [CurrentLocationListener] = []
	
	deinit {
        print("LocationPickerVC DEINIT")
	}
	
	override public func viewDidLoad() {
		super.viewDidLoad()
		
		locationManager.delegate = self
        
        if let tfSearch = self.textFieldSearch{
            let fontSize = CGFloat(Device.size(small: 14, medium: 17, big: 20))
            tfSearch.font = tfSearch.font?.fontWithSize(fontSize)
            tfSearch.delegate = self
            tfSearch.addTarget(self, action: #selector(LocationPickerVC.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        }
        
        if let deleteButton = self.deleteButton{
            deleteButton.addTapGesture(action: { [weak self](UITapGestureRecognizer) in
                self?.textFieldSearch?.text = ""
            })
        }
        
        if let backButton = self.backButton{
            backButton.addTapGesture(action: { [weak self](UITapGestureRecognizer) in
                self?.popVC()
                })
        }
        
        if let table = self.tableView{
            table.dataSource = self
            table.delegate = self
            table.backgroundColor = UIColor.groupTableViewBackgroundColor()
            table.tableFooterView = UIView()
        }
        
	}
    
    override public func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBar.hidden = true
        super.viewWillAppear(animated)
        
        setPopGestureEnabled(true)
    }
    
    public func textFieldDidChange(textField: UITextField) {
        
        sendProxyAction(LocationPickerVCActions.SearchTextChanged.rawValue, payload: nil)
    }

	public func getCurrentLocation() {
		locationManager.requestWhenInUseAuthorization()
		locationManager.startUpdatingLocation()
	}

    // TABLE DELEGATE
    
    public func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textAlignment = .Right
        header.textLabel?.font = sectionHeaderFont
        header.textLabel?.textColor = UIColor.lightGrayColor()
    }
    
    public var tableView:UITableView?{
        return nil
    }
    public var textFieldSearch:UITextField?{
        return nil
    }
    public var deleteButton:UIButton?{
        return nil
    }
    public var backButton:UIButton?{
        return nil
    }
    public var sectionHeaderFont:UIFont{
        return UIFont()
    }
    public var currentLocationCellFont:UIFont{
        return UIFont()
    }
    public var searchAndRecentCellTextFont:UIFont{
        return UIFont()
    }
    public var searchAndRecentCellDetailTextFont:UIFont{
        return UIFont()
    }
    public var searchText:String{
        return "SEARCH"
    }
    public var recentLocationText:String{
        return "RECENT LOCATIONS"
    }
    public var currentLocationText:String{
        return "Current Location"
    }
    public var mapLocationImage:UIImage?{
        return nil
    }
    public var currentLocationArrowImage:UIImage?{
        return nil
    }
    public var mapIconImage:UIImage?{
        return nil
    }
    
    public func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 1 && currentLocations.count > 0{
            return searchText
        }
        if section == 2{
            return recentLocationText
        }
        return nil
    }
    
    public func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }
    
    public func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }
        else if section == 1{
            return currentLocations.count
        }
        return historyLocations.count
    }
    
    public func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("LocationCell")
            ?? UITableViewCell(style: .Subtitle, reuseIdentifier: "LocationCell")
        
        if indexPath.section == 0{
            
            cell.textLabel?.text = currentLocationText
            cell.detailTextLabel?.text = nil
            
            cell.textLabel?.font = currentLocationCellFont
            cell.textLabel?.textColor = UIColor(hexString: "#A89554")
            
            cell.imageView?.contentMode = .ScaleAspectFit
            cell.imageView?.image = mapLocationImage
            
            
            cell.accessoryView?.contentMode = .ScaleAspectFit
            cell.accessoryView?.frame = CGRectMake(0, 0, 15, 17)
            
            let pinIcon = UIImageView(frame: CGRectMake(0, 0, 15, 17))
            pinIcon.image = currentLocationArrowImage
            pinIcon.contentMode = .ScaleAspectFit
            cell.accessoryView = pinIcon
        }
        else{
            cell.textLabel?.font = searchAndRecentCellTextFont
            cell.detailTextLabel?.font = searchAndRecentCellDetailTextFont
            cell.textLabel?.textColor = UIColor.darkGrayColor()
            cell.detailTextLabel?.textColor = UIColor.grayColor()
            
            let location:Location?
            if indexPath.section == 1{
                location = currentLocations[indexPath.row]
            }
            else{
                location = historyLocations[indexPath.row]
            }
            
            cell.textLabel?.text = location?.name
            cell.detailTextLabel?.text = location?.address
            cell.accessoryView = nil
            
            cell.imageView?.contentMode = .ScaleAspectFit
            cell.imageView?.image = mapIconImage
            cell.imageView?.imageTintColor(UIColor.grayColor())
        }
        
        return cell
    }
    
    public func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == 0{
            sendProxyAction(LocationPickerVCActions.SelectLocation.rawValue, payload: nil)
        }
        else if indexPath.section == 1{
            sendProxyAction(LocationPickerVCActions.SelectLocation.rawValue, payload: currentLocations[indexPath.row])
        }
        else if indexPath.section == 2{
            sendProxyAction(LocationPickerVCActions.SelectLocation.rawValue, payload: historyLocations[indexPath.row])
        }
    }
}

extension LocationPickerVC: CLLocationManagerDelegate {
	public func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
		guard let location = locations.first else { return }
		for listener in currentLocationListeners {
			listener.action(location)
		}
		currentLocationListeners = currentLocationListeners.filter { !$0.once }
		manager.stopUpdatingLocation()
	}
}
