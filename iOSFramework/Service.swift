//
//  Service.swift
//  Towers Of Hanoi
//
//  Created by Alexey Honcharov on 08.02.16.
//  Copyright © 2016 We Heart Swift. All rights reserved.
//

import UIKit

public class Service: NSObject {
    public required override init(){
        super.init()
        onInitialized()
    }
    
    final public func getService(serviceClass:AnyClass)->Service{
        return Service.getService(serviceClass)
    }

    public func onInitialized(){
        
    }
    
    public final func dispatchCoreAction(actionName:String, payload:AnyObject?){
        for proxy in Proxy.proxyInstances{
            proxy.onCoreAction(actionName, payload: payload)
        }
        for service in Service.services{
            service.onCoreAction(actionName, payload: payload)
        }
    }
    
    public func onCoreAction(actionType:String, payload:AnyObject?){
        
    }
    
    public static var services = [Service]()
    static public func createService(serviceType: AnyClass)->Service{
        if let serviceClass = serviceType as? Service.Type {
            let service = serviceClass.init()
            services.append(service)
            return service
        }
        else{
            fatalError("Must be Service!")
        }
    }
    
    static public func getService(serviceClass:AnyClass)->Service!{
        
        for service in services{
            if(service.dynamicType == serviceClass){
                return service
            }
        }
        
        let service = Service.createService(serviceClass)
        return service
    }
}
