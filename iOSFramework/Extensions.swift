import UIKit
import SystemConfiguration


extension UIViewController{
    public func hideNavigationBar(){
        self.navigationController?.navigationBar.hidden = true
    }
    public func showNavigationBar(){
        self.navigationController?.navigationBar.hidden = false
    }
    public func navigationBarStyle(style:UIBarStyle = .Default){
        self.navigationController?.navigationBar.barStyle = style
    }
}

extension UIImage {
    static public func resizeImage(image:UIImage, toTheSize size:CGSize)->UIImage{
        let scale = CGFloat(max(size.width/image.size.width,
            size.height/image.size.height))
        let width:CGFloat  = image.size.width * scale
        let height:CGFloat = image.size.height * scale;
        
        let rr:CGRect = CGRectMake( 0, 0, width, height);
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0);
        image.drawInRect(rr)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();
        return newImage
    }
    
    static public func imageWithImage(image:UIImage,scaledToSize newSize:CGSize)->UIImage{
        
        UIGraphicsBeginImageContext( newSize )
        image.drawInRect(CGRect(x: 0,y: 0,width: newSize.width,height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage.imageWithRenderingMode(.AlwaysTemplate)
    }
    
    public func rotateImageByOrientation() -> UIImage {
        // No-op if the orientation is already correct
        guard self.imageOrientation != .Up else {
            return self
        }
        
        // We need to calculate the proper transformation to make the image upright.
        // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
        var transform = CGAffineTransformIdentity;
        
        switch (self.imageOrientation) {
        case .Down, .DownMirrored:
            transform = CGAffineTransformTranslate(transform, self.size.width, self.size.height)
            transform = CGAffineTransformRotate(transform, CGFloat(M_PI))
            
        case .Left, .LeftMirrored:
            transform = CGAffineTransformTranslate(transform, self.size.width, 0)
            transform = CGAffineTransformRotate(transform, CGFloat(M_PI_2))
            
        case .Right, .RightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, self.size.height)
            transform = CGAffineTransformRotate(transform, CGFloat(-M_PI_2))
            
        default:
            break
        }
        
        switch (self.imageOrientation) {
        case .UpMirrored, .DownMirrored:
            transform = CGAffineTransformTranslate(transform, self.size.width, 0)
            transform = CGAffineTransformScale(transform, -1, 1)
            
        case .LeftMirrored, .RightMirrored:
            transform = CGAffineTransformTranslate(transform, self.size.height, 0)
            transform = CGAffineTransformScale(transform, -1, 1)
            
        default:
            break
        }
        
        // Now we draw the underlying CGImage into a new context, applying the transform
        // calculated above.
        let ctx = CGBitmapContextCreate(nil, Int(self.size.width), Int(self.size.height),
                                        CGImageGetBitsPerComponent(self.CGImage), 0,
                                        CGImageGetColorSpace(self.CGImage),
                                        CGImageGetBitmapInfo(self.CGImage).rawValue)
        CGContextConcatCTM(ctx, transform)
        switch (self.imageOrientation) {
        case .Left, .LeftMirrored, .Right, .RightMirrored:
            CGContextDrawImage(ctx, CGRectMake(0,0,self.size.height,self.size.width), self.CGImage)
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,self.size.width,self.size.height), self.CGImage)
        }
        
        // And now we just create a new UIImage from the drawing context
        if let cgImage = CGBitmapContextCreateImage(ctx) {
            return UIImage(CGImage: cgImage)
        } else {
            return self
        }
    }
}


extension UIView {
    
    public func loopRecursivelySubviewsWithFunc(closure:((subview:UIView)->Void)){
        for subView: UIView in self.subviews as [UIView] {
            closure(subview: subView)
            subView.loopRecursivelySubviewsWithFunc(closure)
        }
    }
    
    public func dismissKeyboard(){
        self.endEditing(true)
    }
    
    public func getParentViewController()->UIViewController?{
        let responder = self.nextResponder()
        if let vc = responder as? UIViewController {
            return vc
        }
        else{
            if let vc = superview?.getParentViewController(){
                return vc
            }
        }
        return nil
    }
    
    public func getCurrentFirstResponder() -> AnyObject? {
        if self.isFirstResponder() {
            return self
        }
        
        for subView: UIView in self.subviews as [UIView] {
            if subView.isFirstResponder() {
                return subView
            }
            else {
                if let sub = subView.getCurrentFirstResponder() {
                    return sub;
                }
            }
        }
        return nil
    }
    
    public func getChildLabelWithText(text:String) -> UILabel? {
        if self is UILabel && (self as! UILabel).text == text {
            return self as? UILabel
        }
        
        for subView: UIView in self.subviews as [UIView] {
            if subView is UILabel && (subView as! UILabel).text == text  {
                return subView as? UILabel
            }
            else {
                if let sub = subView.getChildLabelWithText(text) {
                    return sub;
                }
            }
        }
        return nil
    }
    
    public func addCornerRadiusAnimation(from: CGFloat, to: CGFloat, duration: CFTimeInterval)
    {
        let animation = CABasicAnimation(keyPath:"cornerRadius")
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animation.fromValue = from
        animation.toValue = to
        animation.duration = duration
        self.layer.addAnimation(animation, forKey: "cornerRadius")
        self.layer.cornerRadius = to
    }
}
extension UIButton{
    public func imageTintColor(color:UIColor){
        if var image = self.imageView?.image {
            if(image.renderingMode != .AlwaysTemplate){
                image = image.imageWithRenderingMode(.AlwaysTemplate)
                self.setImage(image, forState: UIControlState.Normal)
            }
            self.tintColor = color
        }
    }
}

extension UIImageView{
    public func imageTintColor(color:UIColor){
        if var image = self.image {
            if(image.renderingMode != .AlwaysTemplate){
                image = image.imageWithRenderingMode(.AlwaysTemplate)
                self.image = image
            }
            self.tintColor = color
        }
    }
}

/////////////////


extension NSLayoutConstraint {
    
    override public var description: String {
        let id = identifier ?? ""
        return "id: \(id), constant: \(constant)" //you may print whatever you want here
    }
}

extension Array where Element: Equatable {
    mutating public func removeObject(object: Element?) {
        if(object != nil){
            if let index = self.indexOf(object!) {
                self.removeAtIndex(index)
            }
        }
    }
}

extension String {
    
    //To check text field or String is blank or not
    public var isBlank: Bool {
        get {
            let trimmed = stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
            return trimmed.isEmpty
        }
    }
    
    //validate PhoneNumber
    public var isPhoneNumber: Bool {
        do {
            let detector = try NSDataDetector(types: NSTextCheckingType.PhoneNumber.rawValue)
            let matches = detector.matchesInString(self, options: [], range: NSMakeRange(0, self.characters.count))
            if let res = matches.first {
                return res.resultType == .PhoneNumber && res.range.location == 0 && res.range.length == self.characters.count
            } else {
                return false
            }
        } catch {
            return false
        }
    }
    
    //Is Alphanumeric
    public var isAlphanumeric: Bool {
        do {
            let regex = try NSRegularExpression(pattern: "^[а-яА-Яa-zA-Z0-9]*$", options: .CaseInsensitive)
            return regex.firstMatchInString(self, options: NSMatchingOptions(rawValue: 0), range: NSMakeRange(0, self.characters.count)) != nil
        } catch {
            return false
        }
    }
    
    //Is Numeric
    public var isNumeric: Bool {
        return characters.reduce(true, combine: { (result, value) in
            let string = String(value)
            guard let firstChar = string.utf16.first else {
                return result
            }
            return result && NSCharacterSet.decimalDigitCharacterSet().characterIsMember(firstChar)}
        )
    }
}

extension UIColor {
    convenience public init(hexString: String) {
        let hex = hexString.stringByTrimmingCharactersInSet(NSCharacterSet.alphanumericCharacterSet().invertedSet)
        var int = UInt32()
        NSScanner(string: hex).scanHexInt(&int)
        let a, r, g, b: UInt32
        switch hex.characters.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (1, 1, 1, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}

public struct ScreenSize
{
    static public let SCREEN_WIDTH         = UIScreen.mainScreen().bounds.size.width
    static public let SCREEN_HEIGHT        = UIScreen.mainScreen().bounds.size.height
    static public let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static public let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

public struct DeviceType
{
    static public let isPad = (UIDevice.currentDevice().userInterfaceIdiom == .Pad)
    static public let isPhone = (UIDevice.currentDevice().userInterfaceIdiom == .Phone)
    static public let isTV = (UIDevice.currentDevice().userInterfaceIdiom == .TV)
}


