import UIKit
import Haneke

public let IMAGE_LOADING_DID_END_NOTIFICATION = "imageLoadingDidEndNotification"
public typealias ImageLoaderHandler = (loadedImage:UIImage?) -> (Void)

public class ImageLoader: NSObject {
    
//    static public func getLocalCachedImage(named:String)->UIImage?{
//        if let localeImage = ImageLoader.photoCache.imageWithIdentifier(named){
//            return localeImage
//        }
//        else{
//            if let image = UIImage(named: named){
//                ImageLoader.photoCache.addImage(image, withIdentifier: named)
//                return image
//            }
//        }
//        return nil
//    }
    
    static let imageCache = Shared.imageCache
//    static public func cacheImage(image: Image, urlString: String) {
//        ImageLoader.photoCache.addImage(image, withIdentifier: urlString)
//    }
//    static public func cachedImage(imageID: String) -> Image? {
//        return ImageLoader.photoCache.imageWithIdentifier(imageID)
//    }
    
    var request:NetworkFetcher<UIImage>?
    
    public var loadedImage: UIImage! = nil
    public var caption: String! = nil
    public var index: Int?
    public var loadedImageURL:String?{
        didSet{
            request?.cancelFetch()
            loadedImage = nil
        }
    }
    
    public func cancel(){
        request?.cancelFetch()
        loadedImage = nil
        //self.loadedImageID = nil
    }
    
    public func load(onLoadedCallback:(UIImage -> Void)? = nil) {
        
        request?.cancelFetch()
        
        if (self.loadedImage != nil) {
            onLoadComplete()
            onLoadedCallback?(loadedImage)
            return
        }
        
        if let urlString = loadedImageURL{
            
            let URL = NSURL(string:urlString)!
            request = NetworkFetcher<UIImage>(URL: URL)
            
            Shared.imageCache.fetch(fetcher: request!,
                                    formatName: HanekeGlobals.Cache.OriginalFormatName,
                
                failure: { [weak self](error:NSError?) in
                    self?.loadedImage = nil
                    if(self != nil){
                       onLoadedCallback?(self!.loadedImage)
                    }
                },
                                    
                success: { [weak self](image:UIImage) in
                
                    self?.loadedImage = image
                    self?.onLoadComplete()
                    onLoadedCallback?(image)
                })
        }
    }

    internal func onLoadComplete() {
        NSNotificationCenter.defaultCenter().postNotificationName(IMAGE_LOADING_DID_END_NOTIFICATION, object: self)
    }
}

