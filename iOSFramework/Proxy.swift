import UIKit
import SystemConfiguration
//import Photos

public class Proxy: UIResponder{
    
    static public  var proxyInstances = [Proxy]()
    static public  func createProxyByType(proxyType:AnyClass, owner:CoreController)->Proxy?{
        
        if let proxyClass = proxyType as? Proxy.Type{
            let proxyInstance = proxyClass.init(owner: owner)
            proxyInstances.append(proxyInstance)
            return proxyInstance
        }
        else{
            fatalError("Must be Proxy!")
        }
    }

    public weak var proxyOwner:CoreController?
    
    final public func getService(serviceClass:AnyClass)->Service{
        return Service.getService(serviceClass)
    }
    
    public final func dispatchCoreAction(actionName:String, payload:AnyObject?){
        for proxy in Proxy.proxyInstances{
            proxy.onCoreAction(actionName, payload: payload)
        }
        for service in Service.services{
            service.onCoreAction(actionName, payload: payload)
        }
    }
    
    public required init(owner:CoreController!){
        super.init()
        self.proxyOwner = owner
        
        onInit()
    }
    
    public func onOwnerAction(actionType:String, payload:AnyObject?){
        
    }
    
    public func onCoreAction(actionType:String, payload:AnyObject?){
        
    }
    
    public func onInit(){
        
    }
    
    public func onOwnerWillAppear(){
        
    }
    
    public func onOwnerWillDisappear(){
        
    }
    
    public func onOwnerAppeared(){
        
    }
    
    public func onOwnerDisappeared(){
        
    }
    
    public func prepareForSegue(segue: UIStoryboardSegue) {
        
    }
    
    // APP DELEGATE
    
    public func applicationWillTerminate(){
        
    }
    
    public func applicationDidBecomeActive(){
        
    }
    
    public func applicationDidEnterBackground(){
        
    }
    
    // UTILS
    
    public final func dismissKeyboard(){
        proxyOwner?.dismissKeyboard()
    }
}
