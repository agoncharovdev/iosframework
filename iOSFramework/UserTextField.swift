public enum UserTextFieldValidationState:String{
    case Valid = "Valid", Invalid = "Invalid", Default = "Default", None = "None"
}

public class UserTextField:FloatLabelTextField{
    
    var bottomLine = CALayer()
    public var validateCallback:((tf:UserTextField)->Void)?
    
    override public func setup() {
        super.setup()
        
        self.borderStyle = UITextBorderStyle.None;
        
        let width = CGFloat(1.0)
        
        bottomLine.borderWidth = width
        self.layer.addSublayer(bottomLine)
        
        //self.clipsToBounds = false
        validationState = .Default
        
        switch self.keyboardType{
        case .NumberPad, .PhonePad:
            addDoneButton()
            break
        default:break
        }
    }
    
    public func addDoneButton(){
        
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action: #selector(UserTextField.didTapDone(_:)))
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        self.inputAccessoryView = keyboardToolbar
    }
    
    public func didTapDone(sender: AnyObject?) {
        if let delegate = self.delegate as? CoreController{
            delegate.view.endEditing(true)
        }
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        bottomLine.frame = CGRect(x: 0, y: self.frame.size.height/1.2,   width:  self.frame.size.width, height: 1)
    }
    
    public func onEditingBegin(){
        validateCallback?(tf: self)
    }
    
    public func onEditingEnd(){
        validateCallback?(tf: self)
    }
    
    public var validationState:UserTextFieldValidationState = .Default{
        didSet{
            switch validationState{
            case .Default:
                bottomLine.borderColor = UIColor(hexString: "#cccccc").CGColor
                break
            case .Invalid:
                bottomLine.borderColor = UIColor(hexString: "#E8668D").CGColor
                break
            case .Valid:
                bottomLine.borderColor = UIColor(hexString: "#66E8C1").CGColor
                break
            case .None:
                bottomLine.borderColor = UIColor.clearColor().CGColor
                break
            }
        }
    }
}