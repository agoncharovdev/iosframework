//
//  MVCDataStorageService.swift
//  Yammy
//
//  Created by Alexey Honcharov on 08.02.16.
//  Copyright © 2016 Alexey Honcharov. All rights reserved.
//

import UIKit
import CoreData

public class StorageService: Service {
    
    public func storageName() -> String{
        fatalError("Must be overrided")
    }
    
    final public func loadSingleEntity(entityName:String, entityClass:AnyClass) -> NSManagedObject? {
        
        if let entityDescription = getEntityDescription(entityName) {
            
            let fetchRequest = NSFetchRequest()
            fetchRequest.entity = entityDescription
            
            do{
                let result = try managedObjectContext.executeFetchRequest(fetchRequest)
                print("\(entityName) cout = \(result.count)")
                
                let entity:NSManagedObject?
                if(result.count > 0){
                    entity =  result[0] as? NSManagedObject
                }
                else{
                    entity = createEntity(entityName, entityClass: entityClass)
                    try managedObjectContext.save()
                }
                return entity
            }
            catch let error as NSError{
                print("Fetch error \(error), \(error.userInfo)")
            }
        }
        
        return nil
    }
    
    final public func createEntity(entityName:String, entityClass:AnyClass)->NSManagedObject{
        
        let entityDescription = getEntityDescription(entityName)!
        let entity = (entityClass as! NSManagedObject.Type).init(entity:entityDescription, insertIntoManagedObjectContext: managedObjectContext)
        return entity
    }
    
    final public func deleteEntity(entity:NSManagedObject?) -> Bool {
        
        if(entity != nil){
            do{
                managedObjectContext.deleteObject(entity!)
                try managedObjectContext.save()
                print("Entity deleted!")
                return true
            }
            catch let error as NSError{
                print("Fetch error \(error), \(error.userInfo)")
            }
        }
        
        return false
    }
    
    final public func getAllEntitiesByName(entityName:String) -> [AnyObject]? {

        if let entityDescription = getEntityDescription(entityName) {

            let fetchRequest = NSFetchRequest()
            fetchRequest.entity = entityDescription

            do{
                let result = try managedObjectContext.executeFetchRequest(fetchRequest)
                return result
                
                // how much entities instances of such type is stored
                //    let fetchRequest = NSFetchRequest()
                //    fetchRequest.predicate = NSPredicate(format: "%K==%@", "name", userModel.userName)
                //    fetchRequest.entity = entityDescription
            }
            catch let error as NSError{
                print("Fetch error \(error), \(error.userInfo)")
            }
        }

        return nil
    }

    final public func deleteAllEntitiesByName(entityName:String) -> Bool {

        if let entityDescription = getEntityDescription(entityName) {

            let fetchRequest = NSFetchRequest()
            fetchRequest.entity = entityDescription

            do{
                let result = try managedObjectContext.executeFetchRequest(fetchRequest)
                if(result.count < 1){
                    print("No Entity for delete!")
                    return true
                }

                for managedObj in result{
                    managedObjectContext.deleteObject(managedObj as! NSManagedObject)
                }
                //let managedObj = result.first
                
                try managedObjectContext.save()
                print("All entities \(entityName) were deleted!")
                return true
            }
            catch let error as NSError{
                print("Fetch error \(error), \(error.userInfo)")
            }
        }

        return false
    }
    
    // MARK: - Core Data Saving support
    final public func saveContext () {
        
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
    
    private func getEntityDescription(entityName:String) -> NSEntityDescription? {
        
        let entityDescription = NSEntityDescription.entityForName(entityName, inManagedObjectContext: managedObjectContext)
        
        if (entityDescription == nil) {
            fatalError("There is no EntityDescription with such Name!")
        }
        
        return entityDescription
    }
    
    
    // MARK: - Core Data stack
    
    private lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.virer.LoginTest" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1]
    }()
    
    private lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let storageName = self.storageName()
        let modelURL = NSBundle.mainBundle().URLForResource(storageName, withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
    }()
    
    private lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let storageName = self.storageName()
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent(storageName)
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason
            
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()
    
    private lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
}
